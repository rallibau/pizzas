package com.rallibau.models;

import java.io.Serializable;

public interface Entity extends Serializable {
	Long getId();
}
