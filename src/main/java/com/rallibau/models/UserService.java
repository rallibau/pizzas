package com.rallibau.models;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

	User findUserByAccessToken(String accessToken);

	AccessToken createAccessToken(User user);

}
