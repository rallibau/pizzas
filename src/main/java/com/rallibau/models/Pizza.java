package com.rallibau.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;



@Entity

@Table(name = "pizzas")
public class Pizza {
	@Id
	@Column(name = "PIZ_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence_pizza")
	@SequenceGenerator(name = "id_Sequence_pizza", sequenceName = "PIZZA_SEQ")
	private long id;
	@Column(name = "PIZ_NOMBRE", unique = false, nullable = false, length = 100)
	private String nombre;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "PIZ_ID")
	private List<Ingrediente> ingredientes;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonIgnore
	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}

}
