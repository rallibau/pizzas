package com.rallibau.controller;

//https://github.com/philipsorst/angular-rest-springsecurity/blob/master/src/main/webapp/js/app.js

import org.hibernate.Session;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rallibau.models.AccessToken;
import com.rallibau.models.User;

@RestController
public class LoginController {
	
	@RequestMapping(value = "/login", method = RequestMethod.GET, produces = { "application/xml",
	"application/json" }, headers = "Accept=application/json")
	public AccessToken login(String usuario,String pwd) {
		AccessToken token =null;
		Session session  = null;
		try {
			if(usuario.equals("test") && pwd.equals("pwd")){
				token = new AccessToken(new User("test", "pwd"), "tokenTest");
			}
		} catch (Exception e) {
			token =null;
			e.printStackTrace();
		}finally {
			if(session!=null){
				session.close();
			}
		}
		return token;

	}
}
