package com.rallibau.controller;

import java.util.List;

import org.hibernate.Session;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.rallibau.models.Pizza;
import com.rallibau.util.HibernateUtil;

@RestController
public class MainController {

	/**
	 * Obtiene las tareas de un usuario
	 * 
	 * @param user
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/pizzas", method = RequestMethod.GET, produces = { "application/xml",
			"application/json" }, headers = "Accept=application/json")
	public @ResponseBody List<Pizza> getPizzas() {
		List<Pizza> pizzas = null;
		Session session  = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			pizzas = session.createCriteria(Pizza.class).list();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(session!=null){
				session.close();
			}
		}
		return pizzas;

	}

	/**
	 * Detalle de una tarea
	 * 
	 * @param taskId
	 * @return
	 */
	@RequestMapping(value = "/pizzas/add", method = RequestMethod.POST, produces = { "application/xml",
			"application/json" }, headers = "Accept=application/json")
	public Pizza savePizza(@RequestBody Pizza pizza) {
		Session session  = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(pizza);
			session.getTransaction().commit();
		} catch (Exception e) {
			pizza =null;
			e.printStackTrace();
		}finally {
			if(session!=null){
				session.close();
			}
		}
		
		return pizza;

	}

	/**
	 * Inicia una tarea de un usuario
	 * 
	 * @param taskId
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/pizzas/{idPizza}", method = RequestMethod.GET, produces = { "application/xml",
			"application/json" }, headers = "Accept=application/json")
	public Pizza getPizza(@PathVariable(value = "idPizza") Long idPizza) {
		Pizza pizza =null;
		Session session  = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			pizza=(Pizza)session.get(Pizza.class, idPizza);
		} catch (Exception e) {
			pizza =null;
			e.printStackTrace();
		}finally {
			if(session!=null){
				session.close();
			}
		}
		return pizza;

	}

}
