package com.rallibau.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.rallibau.models.Pizza;

public class HibernateUtil {
	private static SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			if (sessionFactory == null) {
				Configuration configuration = new Configuration()
						.configure(HibernateUtil.class.getResource("/META-INF/persistence.xml"));
				StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
				serviceRegistryBuilder.applySettings(configuration.getProperties());
				ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
				// datos por defecto

				Session session = sessionFactory.openSession();
				try {
					session.beginTransaction();
					
					Pizza pizza = new Pizza();
					pizza.setNombre("Margarita");
					session.save(pizza);
					
					pizza = new Pizza();
					pizza.setNombre("Barbacoa");
					session.save(pizza);
					
					pizza = new Pizza();
					pizza.setNombre("Carbonada");
					session.save(pizza);
					
					
					session.getTransaction().commit();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (session != null) {
						session.close();
					}
				}
			}
			return sessionFactory;
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		getSessionFactory().close();
	}
}
