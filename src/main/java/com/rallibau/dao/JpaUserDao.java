package com.rallibau.dao;

import java.util.List;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.rallibau.models.User;

public class JpaUserDao extends JpaDao<User, Long> implements UserDao {

	public JpaUserDao() {
		super(User.class);
	}

	public List<User> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public User find(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public User save(User entity) {
		// TODO Auto-generated method stub
		return null;
	}

	public void delete(Long id) {
		// TODO Auto-generated method stub

	}

	public void delete(User entity) {
		// TODO Auto-generated method stub

	}

	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	public User findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
