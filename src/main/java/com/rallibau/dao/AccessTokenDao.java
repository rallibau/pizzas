package com.rallibau.dao;

import com.rallibau.models.AccessToken;

public interface AccessTokenDao extends Dao<AccessToken, Long> {
	AccessToken findByToken(String accessTokenString);
}