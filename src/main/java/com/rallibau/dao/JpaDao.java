package com.rallibau.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.rallibau.models.Entity;

public class JpaDao<T extends Entity, I> implements Dao<T, I> {
	private EntityManager entityManager;

	protected Class<T> entityClass;

	public JpaDao(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public EntityManager getEntityManager() {
		return this.entityManager;
	}

	@PersistenceContext
	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public List<T> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public T find(I id) {
		// TODO Auto-generated method stub
		return null;
	}

	public T save(T entity) {
		// TODO Auto-generated method stub
		return null;
	}

	public void delete(I id) {
		// TODO Auto-generated method stub
		
	}

	public void delete(T entity) {
		// TODO Auto-generated method stub
		
	}

}
