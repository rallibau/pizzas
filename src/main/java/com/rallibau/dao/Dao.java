package com.rallibau.dao;


import java.util.List;

public interface Dao<T extends com.rallibau.models.Entity, I>
{
    List<T> findAll();

    T find(I id);

    T save(T entity);

    void delete(I id);

    void delete(T entity);
}