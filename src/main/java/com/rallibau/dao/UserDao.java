package com.rallibau.dao;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.rallibau.models.User;

public interface UserDao extends Dao<User, Long> {
	User loadUserByUsername(String username) throws UsernameNotFoundException;

	User findByName(String name);
}
