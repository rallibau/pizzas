package com.rallibau.config;

import org.springframework.beans.factory.annotation.Value;


public class AppConfig {
	@Value("${api.url}")
	private String digitalizatAPI;

	public String getDigitalizatAPI() {
		return digitalizatAPI;
	}

	public void setDigitalizatAPI(String digitalizatAPI) {
		this.digitalizatAPI = digitalizatAPI;
	}
	
}
