var app = angular.module("app", [ "ngMaterial", "ngRoute" ]);
app.config(function($routeProvider, $httpProvider) {
	$routeProvider.when("/", {
		templateUrl : "pages/listado.html",
		controller : "listadoController"

	}).when("/detalle/:pizza_id", {
		templateUrl : "pages/detalle.html",
		controller : "detalleController"
	}).when("/add", {
		templateUrl : "pages/add.html",
		controller : "addController"
	}).when("/login", {
		templateUrl : "pages/login.html",
		controller : "loginController"
	}).otherwise({
		redirectTo : '/'
	});
	$httpProvider.interceptors.push(function($q, $rootScope, $location) {
		return {
			'request' : function(config) {
				if (angular.isDefined($rootScope.accessToken)) {
					var accessToken = $rootScope.accessToken;
					config.headers['X-Access-Token'] = accessToken;
				}
				return config || $q.when(config);
			}
		};
	});
});
app.controller('appCtrl', function($scope, $mdBottomSheet, $timeout,
		$mdSidenav, $rootScope, $location) {
	$scope.toggleLeft = buildToggler('left');
	$scope.cerrarToggle = function() {
		$mdSidenav('left').close();
	}

	function buildToggler(componentId) {
		return function() {
			$mdSidenav(componentId).toggle();
		};
	}
	
	
	$scope.cerrarSesion = function(){
		$rootScope.accessToken = undefined;
		$location.path('/login');
	};
	$scope.loged = function(){
		if (angular.isDefined($rootScope.accessToken)) {
			return true;
		}else{
			return false;
		}
	};

});

app.controller('detalleController', function($scope, $http, $routeParams,
		$rootScope, $location) {
	console.log("detalle");
	console.log($routeParams.pizza_id);
	if (!angular.isDefined($rootScope.accessToken)) {
		$location.path('/login');
	} else {

		$http({
			method : 'GET',
			url : '/pizzas/' + $routeParams.pizza_id
		}).then(function successCallback(response) {
			console.log(response.data);
			$scope.pizza = response.data;
		}, function errorCallback(response) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
		});
	}
});

app.controller('addController', function($scope, $http, $location,$rootScope) {
	console.log("add");
	if (!angular.isDefined($rootScope.accessToken)) {
		$location.path('/login');
	} else {
		$scope.submit = function() {
			var data = {
				nombre : $scope.nombre
			};
			$http.post('/pizzas/add', JSON.stringify(data)).then(
					function successCallback(response) {
						console.log("done!!!");
						$location.path('/')
					}, function errorCallback(response) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});
		};
	}
});
app.controller('listadoController', function($scope, $http, $rootScope,
		$location) {
	console.log("listado");
	if (!angular.isDefined($rootScope.accessToken)) {
		$location.path('/login');
	} else {
		$http({
			method : 'GET',
			url : '/pizzas'
		}).then(function successCallback(response) {
			console.log(response.data);
			$scope.pizzas = response.data;
		}, function errorCallback(response) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
		});
	}

});

app.controller('loginController',
		function($scope, $http, $location, $rootScope) {
			console.log("login");
			$scope.submit = function() {
				var data = {
					usuario : $scope.nombre,
					pwd : $scope.pwd
				};
				$http({
					method : 'GET',
					url : '/login',
					params : data
				}).then(function successCallback(response) {
					console.log(response.data.token);
					if (response.data.token != undefined) {
						$location.path('/');
						$rootScope.accessToken = response.data.token;
					} else {
						alert("Incorrecto");
					}

				}, function errorCallback(response) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});
			};

		});